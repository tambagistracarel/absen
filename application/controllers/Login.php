<?php

class Login extends CI_Controller
{
	public function index()
	{
		$data["page"] = "login/v_login";
		$this->load->view("layout/login", $data);
	}

	public function prosesLogin()
	{
		$username = $this->input->post("username", true);
		$password = $this->input->post("password", true);
		$base64auth = base64_encode($username . ":" . $password);
		$result = apiLogin("login", $base64auth);
		$kode = $result["kode"];
		if ($kode == 201) {
			$dataUser = $result["body"];
			$dataSession = array(
				"id_dosen" => $dataUser,
				"nomor_dosen" => $dataUser,
				"nama_dosen" => $dataUser,
				"is_dosen" => $dataUser,
				"token" => $dataUser
			);
			$this->session->set_userdata($dataSession);
			echo "1";
		} else {
			echo "Username dan Password tidak ditemukan";
		}
		
	}
}

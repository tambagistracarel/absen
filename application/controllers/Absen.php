<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absen extends CI_Controller
{
	public function index()
	{
		$result = apiRequest("dosen/matakuliah-ajar", "GET");
		$body = $result["body"]->data;
		$data = array(
			"header" => "List Absen",
			"page" => "content/v_list_absen",
			"absens" => $body
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function proses_update()
	{
		$id = $this->input->post('dataIdPertemuan');
		$qrkode = $this->input->post('dataQrCode');
		$params = array(
			"qr" => $qrkode,
			"id_absen_dosen" => $id,
		);
		$result = apiRequest("dosen/matakuliah-ajar?" . http_build_query($params), "PUT");
		return $result;
	}

	public function listPertemuan($idMtKul)
	{
		$params = array(
			"id_dosen_semester" => $idMtKul
		);
		$id = $this->input->post('dataIdPertemuan');
		$qrkode = $this->input->post('dataQrCode');
		$params1 = array(
			"qr" => $qrkode,
			"id_absen_dosen" => $id,
		);
		$result = apiRequest("dosen/matakuliah-ajar?" . http_build_query($params1), "PUT");
		$result = apiRequest("dosen/materiajar?" . http_build_query($params), "GET");
		$listData = $result["body"]->data;
		$data = array(
			"header" => "List Materi",
			"pertemuan" => $listData,
			"page" => "content/v_list_pertemuan"
		);
		$this->load->view("layout/dashboard", $data);
	}
}

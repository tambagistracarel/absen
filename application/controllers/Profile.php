<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

    public function index()
    {
        $result = apiRequest("dosen/profile", "GET");
        // var_dump($result);
        // die();
        $body = $result["body"]->data;
        $data = array(
            "header" => "Profile Dosen",
            "profiles" => $body,
            "page" => "profile/v_profile"
        );
        $this->load->view("layout/dashboard", $data);
    }
}

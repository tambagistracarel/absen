<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark"><?= $header ?></h1>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<section class="content">
		<div class="card card-info">
			<div class="card-footer">
				<div class="card-body">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="text-align:center">No.</th>
								<th style="text-align:center">No. Matakuliah</th>
								<th style="text-align:center">Kelas</th>
								<th style="text-align:center">Nama Matakuliah</th>
								<th style="text-align:center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($absens as $a) {
							?>
								<tr>
									<td style="text-align:center"><?= $no++ ?></td>
									<td style="text-align:center"><?= $a->no_mt_kul ?></td>
									<td style="text-align:center"><?= $a->kelas ?></td>
									<td style="text-align:center"><?= $a->nama_mt_kul ?></td>
									<td style="text-align:center">
										<input type="hidden" id="qrkey" name="qr-key" value="<?= $a->no_dosen ?>" />
										<!-- <a href="#" class="btn btn-sm btn-primary" id="buka-qrcode" data-no-makul="<?= $a->no_mt_kul ?>" data-kelas="<?= $a->kelas ?>" data-makul="<?= $a->nama_mt_kul ?>" value="<?= $a->id_dosen ?>" title="Edit"><i class="glyphicon glyphicon-pencil"></i> Buka</a> -->
										<a href="<?= site_url(array("absen", "listPertemuan", $a->id)) ?>" class="btn bg-blue">
											List Materi</a>
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
	</section>
</div>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark"><?= $header ?></h1>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<section class="content">
		<div class="card card-info">
			<div class="card-footer">
				<div class="card-body">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="text-align:center">No.</th>
								<th style="text-align:center">Materi</th>
								<th style="text-align:center">Tanggal</th>
								<th style="text-align:center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($pertemuan as $a) {
							?>
								<tr>
									<td style="text-align:center"><?= $no++ ?></td>
									<td style="text-align:center"><?= $a->materi_absen_dosen ?></td>
									<td style="text-align:center"><?= $a->tanggal_absen_dosen ?></td>
									<td style="text-align:center">
										<input type="hidden" id="qrkey" name="qr-key" value="<?= $a->id_absen_dosen ?>" />
										<a href="#" class="btn btn-sm btn-primary" id="buka-qrcode" data-idAbDos="<?= $a->id_absen_dosen ?>" data-materi="<?= $a->materi_absen_dosen ?>" data-tanggal="<?= $a->tanggal_absen_dosen ?>" value="<?= $a->id_absen_dosen ?>" title="Edit"><i class="glyphicon glyphicon-pencil"></i> Buka</a>
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
	</section>
</div>
<div class="modal fade" id="modal-qrcode" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<input type="hidden" id="edit_record_id" name="edit_record_id" value="" />
				<input type="hidden" id="edit_record_idAbDos" name="edit_record_idAbDos" value="" />
				<input type="hidden" id="edit_record_materi" name="edit_record_materi" value="" />
				<input type="hidden" id="edit_record_tanggal" name="edit_record_tanggal" value="" />
				<div id="qrcode" name="qr_code"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<script src="<?= base_url(); ?>assets_dashboard/plugins/qr-code/qrcode.min.js"></script>
<script src="<?= base_url(); ?>assets_dashboard/plugins/qr-code/qrcode.js"></script>
<script src="<?= base_url(); ?>assets_dashboard/plugins/js/md5.min.js"></script>

<script>
	$(document).on("click", "#buka-qrcode", function(e) {
		e.preventDefault();
		var pertemuan_id = $(this).attr("value");
		var idAbDos = $(this).data('idAbDos');
		var materi = $(this).data('materi');
		var tanggal = $(this).data('tanggal');
		$("#edit_record_id").val(pertemuan_id);
		$("#edit_record_materi").val(materi);
		$("#edit_record_tanggal").val(tanggal);
		$("#qrcode").val(materi);
		$('#modal-qrcode').modal('show');
		refreshQRCode();
	});

	var qrdata = document.getElementById('qr-data');
	let convert = "";
	var qrcode = new QRCode(document.getElementById('qrcode'));
	let time;


	function refreshQRCode() {
		var idpertemuan = document.getElementById('edit_record_id').value;
		var materi = document.getElementById('edit_record_materi').value;
		var tanggal = document.getElementById('edit_record_tanggal').value;
		var kelas = document.getElementById('qrcode').value;
		var date = new Date().getSeconds();
		var all = idpertemuan + "-" + materi + "-" + tanggal + "-" + date;
		// convert = md5(nomakul);
		qrcode.makeCode(all);
		setTimeout(refreshQRCode, 1000 * 10);

		$.ajax({
			url: "Absen/listPertemuan".idpertemuan,
			type: "POST",
			data: {
				dataIdPertemuan: idpertemuan,
				dataQrCode: all
			},
			dataType: "json",
			success: function() {}
		});

	}
</script>
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark"><?= $header ?></h1>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<section class="content">
		<div class="card card-info">
			<div class="card-footer">
				<div class="card-body">
					<table id="example1" class="table">
						<tbody>
							<tr>
								<td style="text-align:left">No. Dosen</td>
                <td>&nbsp;:&nbsp;</td>
								<td style="text-align:left"><?= $profiles->nomor_dosen ?></td>
							</tr>
							<tr>
								<td style="text-align:left">Nama Dosen</td>
                <td>&nbsp;:&nbsp;</td>
								<td style="text-align:left"><?= $profiles->nama_dosen ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
	</section>
	<!-- /.content -->
</div>

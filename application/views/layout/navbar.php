<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
    </li>
  </ul>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#">
        <i class="fa fa-user"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <a href="<?= site_url('Profile') ?>" class="dropdown-item">
          <i class="fa fa-user"></i> Profile
        </a>
        <a href="<?= site_url('Login') ?>" class="dropdown-item">
          <i class="fa fa-power-off"></i> Logout
        </a>
      </div>
    </li>
  </ul>
</nav>
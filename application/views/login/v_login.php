<form id="form-login" class="login100-form validate-form">
	<span class="login100-form-title p-b-33">
		Login Dosen
	</span>
	<div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
		<input class="input100" type="text" name="username" placeholder="NIDN">
		<span class="focus-input100-1"></span>
		<span class="focus-input100-2"></span>
	</div>

	<div class="wrap-input100 rs1 validate-input" data-validate="Password is required">
		<input class="input100" type="password" name="password" placeholder="Password">
		<span class="focus-input100-1"></span>
		<span class="focus-input100-2"></span>
	</div>

	<!--	<div class="container-login100-form-btn m-t-20">-->
	<button id="btn-login" type="button" class="login100-form-btn">
		Sign in
	</button>
	<!--	</div>-->
</form>
<script>
	$(function () {
		$("#btn-login").on("click", function () {
			let data = $("form").serialize();
			swal.fire({
				title: "Processing Data..",
				text: "Data sedang berkelana",
				imageUrl: '<?= base_url() ?>' + "assets_login/images/loading.gif",
				showConfirmButton: false,
				allowOutsideClick: false
			});
			$.ajax({
				type: "POST",
				url: '<?= base_url() ?>' + 'login/prosesLogin',
				data: data, // serializes the form's elements.
				success: function (data) {
					if (data === "1") {
						window.location.replace("<?= site_url("Welcome ") ?>");
					} else {
						Swal.fire("Input Data Salah", data, "error");
					} // show response from the php script.
				}
			});
		});
	});

</script>
